package cas_test

import (
	"testing"

	"gitlab.com/stone.code/cas"
)

func TestError_Error(t *testing.T) {
	cases := []struct {
		err string
	}{
		{"mock"},
		{"dummy"},
	}

	for _, v := range cases {
		t.Run(v.err, func(t *testing.T) {
			if out := cas.Error(v.err).Error(); out != v.err {
				t.Errorf("wanted %v, got %v", v.err, out)
			}
		})
	}
}
