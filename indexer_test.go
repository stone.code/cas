package cas_test

import (
	"context"
	"io"
	"reflect"
	"testing"

	"gitlab.com/stone.code/cas"
	"gitlab.com/stone.code/cas/memory"
)

type mockStorage struct{}

func (*mockStorage) Close() error {
	return nil
}

func (*mockStorage) Create(src io.Reader) (cas.ID, error) {
	return cas.ID{}, nil
}

func (*mockStorage) Open(id cas.ID) (io.ReadCloser, error) {
	return nil, nil
}

type mockIndexer struct{}

func (*mockIndexer) Index(ctx context.Context) (<-chan cas.ID, error) {
	ch := make(chan cas.ID)

	go func() {
		defer close(ch)

		ids := []cas.ID{
			{0x94, 0x3a, 0x70, 0x2d, 0x06, 0xf3, 0x45, 0x99, 0xae, 0xe1, 0xf8, 0xda, 0x8e, 0xf9, 0xf7, 0x29, 0x60, 0x31, 0xd6, 0x99},
			{0xa0, 0xf2, 0x35, 0xd6, 0x8c, 0xce, 0x3a, 0xe7, 0x4c, 0x7f, 0xcb, 0xad, 0x60, 0xea, 0xdc, 0x22, 0x61, 0x52, 0xcb, 0x3d},
			{0xda, 0x39, 0xa3, 0xee, 0x5e, 0x6b, 0x4b, 0x0d, 0x32, 0x55, 0xbf, 0xef, 0x95, 0x60, 0x18, 0x90, 0xaf, 0xd8, 0x07, 0x09},
		}

		for _, id := range ids {
			ch <- id
		}
	}()

	return ch, nil
}

type emptyStorage struct {
	mockStorage
	mockIndexer
}

func (*emptyStorage) Open(id cas.ID) (io.ReadCloser, error) {
	return memory.NewReader(nil), nil
}

type openfailStorage struct {
	mockStorage
	mockIndexer
}

func (*openfailStorage) Open(id cas.ID) (io.ReadCloser, error) {
	return nil, errMockOpen
}

type readfailStorage struct {
	mockStorage
	mockIndexer
}

func (*readfailStorage) Open(id cas.ID) (io.ReadCloser, error) {
	return &errorReadCloser{}, nil
}

type errorReadCloser struct{}

func (r *errorReadCloser) Close() error {
	return nil
}

func (r *errorReadCloser) Read(b []byte) (int, error) {
	return 0, errMockRead
}

type indexfailStorage struct {
	mockStorage
}

func (*indexfailStorage) Index(ctx context.Context) (<-chan cas.ID, error) {
	return nil, errMockIndex
}

const (
	errMockOpen  cas.Error = "mock open error"
	errMockRead  cas.Error = "mock read error"
	errMockIndex cas.Error = "mock index error"
)

func TestVerifyStorage(t *testing.T) {
	// Initialize an in-memory store.
	storage := memory.New()
	defer storage.Close()

	// Insert some random content into the store.
	blobs := []string{"", "Hello, Worlds!", "0xdeadbeaf"}
	for _, v := range blobs {
		_, err := cas.CreateFromString(storage, "")
		if err != nil {
			t.Fatalf("could not add content \"%s\": %s", v, err)
		}
	}

	// Verify the contents.
	ids := []cas.ID{}
	err := cas.VerifyStorage(nil, storage, func(id cas.ID) bool {
		ids = append(ids, id)
		return true
	})
	if err != nil {
		t.Errorf("could not verify storage: %s", err)
	}
	if len(ids) != 0 {
		t.Errorf("corruption of the storage detected %v", ids)
	}

	// Check that the proper error is returned if the the storage does not
	// support the Indexer interface.
	t.Run("mock", func(t *testing.T) {
		ids := []cas.ID{}
		err := cas.VerifyStorage(nil, &mockStorage{}, func(id cas.ID) bool {
			ids = append(ids, id)
			return true
		})
		if err != cas.ErrNotIndexer {
			t.Errorf("got %v", err)
		}
		if len(ids) != 0 {
			t.Errorf("got %v", ids)
		}
	})

	// Check that mismatch between countents and ID can be detected.
	t.Run("empty", func(t *testing.T) {
		ids := []cas.ID{}
		err := cas.VerifyStorage(nil, &emptyStorage{}, func(id cas.ID) bool {
			ids = append(ids, id)
			return true
		})
		if err != nil {
			t.Errorf("got %v", err)
		}
		want := []cas.ID{
			{0x94, 0x3a, 0x70, 0x2d, 0x06, 0xf3, 0x45, 0x99, 0xae, 0xe1, 0xf8, 0xda, 0x8e, 0xf9, 0xf7, 0x29, 0x60, 0x31, 0xd6, 0x99},
			{0xa0, 0xf2, 0x35, 0xd6, 0x8c, 0xce, 0x3a, 0xe7, 0x4c, 0x7f, 0xcb, 0xad, 0x60, 0xea, 0xdc, 0x22, 0x61, 0x52, 0xcb, 0x3d},
		}
		if !reflect.DeepEqual(ids, want) {
			t.Errorf("got %v", ids)
		}
	})

	// Check that mismatch between countents and ID can be detected.
	// Early return.
	t.Run("empty", func(t *testing.T) {
		id := cas.ID{}
		err := cas.VerifyStorage(nil, &emptyStorage{}, func(found cas.ID) bool {
			id = found
			return false
		})
		if err != nil {
			t.Errorf("got %v", err)
		}
		want := []cas.ID{
			{0x94, 0x3a, 0x70, 0x2d, 0x06, 0xf3, 0x45, 0x99, 0xae, 0xe1, 0xf8, 0xda, 0x8e, 0xf9, 0xf7, 0x29, 0x60, 0x31, 0xd6, 0x99},
			{0xa0, 0xf2, 0x35, 0xd6, 0x8c, 0xce, 0x3a, 0xe7, 0x4c, 0x7f, 0xcb, 0xad, 0x60, 0xea, 0xdc, 0x22, 0x61, 0x52, 0xcb, 0x3d},
		}
		if !reflect.DeepEqual(id, want[0]) && !reflect.DeepEqual(id, want[1]) {
			t.Errorf("got %v", id)
		}
	})

	// Check that error in opening contents is reported.
	t.Run("sad open", func(t *testing.T) {
		ids := []cas.ID{}
		err := cas.VerifyStorage(nil, &openfailStorage{}, func(id cas.ID) bool {
			ids = append(ids, id)
			return true
		})
		if err != errMockOpen {
			t.Errorf("got %v", err)
		}
		if len(ids) != 0 {
			t.Errorf("got %v", ids)
		}
	})

	// Check that error in reading contents is reported.
	t.Run("sad read", func(t *testing.T) {
		ids := []cas.ID{}
		err := cas.VerifyStorage(nil, &readfailStorage{}, func(id cas.ID) bool {
			ids = append(ids, id)
			return true
		})
		if err != errMockRead {
			t.Errorf("got %v", err)
		}
		if len(ids) != 0 {
			t.Errorf("got %v", ids)
		}
	})

	// Check that error in indexing contents is reported.
	t.Run("sad index", func(t *testing.T) {
		ids := []cas.ID{}
		err := cas.VerifyStorage(nil, &indexfailStorage{}, func(id cas.ID) bool {
			ids = append(ids, id)
			return true
		})
		if err != errMockIndex {
			t.Errorf("got %v", err)
		}
		if len(ids) != 0 {
			t.Errorf("got %v", ids)
		}
	})
}
