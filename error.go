package cas

// Error records errors that are common across all concrete implementations of
// Storage.
type Error string

// Common errors that should be used by all concrete implementations of
// Storage.
const (
	ErrNotExist   Error = "object with ID does not exist"
	ErrNotRemover Error = "storage does not support removing of contents"
	ErrNotIndexer Error = "storage does not support indexing of contents"
)

// Error returns the message associated with the error.
func (e Error) Error() string {
	return string(e)
}
