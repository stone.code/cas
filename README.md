# cas
> Content addressible storage in Go.

Package cas provides a framework for working with Content Addressible Storage 
(CAS).

## Installation

As with most projects for [Go](https://golang.org), installation is as easy as `go get`.

## Documentation

Please refer to the documentation on [godoc.org](https://godoc.org/gitlab.com/stone.code/cas) for API documentation.

## Contributing

Development of this project is ongoing.  If you find a bug or have any suggestions, please [open an issue](https://gitlab.com/stone.code/cas/issues).

If you'd like to contribute, please fork the repository and make changes.  Pull requests are welcome.

## Related projects and documentation

In progress

## Licensing

This project is licensed under the [3-Clause BSD License](https://opensource.org/licenses/BSD-3-Clause).  See the LICENSE in the repository.
