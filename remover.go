package cas

// RemoveStorage is an extension interface for a content-addressable storage
// (CAS) that can remove items.
type RemoveStorage interface {
	Storage

	// Remove deletes the content associated with the id.
	Remove(id ID) error
}
