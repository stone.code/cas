package cas

import "context"

// IndexStorage is an extension interface for a content-addressable storage
// (CAS) that can list its contents.
type IndexStorage interface {
	// Index will concurrently return all of the IDs currently in the storage.
	// The completeness or accuracy of the list is undefined when mixed with
	// concurrent access to the storage.
	Index(ctx context.Context) (<-chan ID, error)
}

// VerifyStorage scans the objects in the storage, and checks that the contents
// have not been corrupted.  If there is concurrent access to the storage, some
// objects may not be checked.
//
// A callback will be called for every mismatch detected.  The ID will indicate
// the item for which the contents don't match.  The callback should return
// true to indicate that the search should continue.
//
// The storage must also support the IndexStorage interface.
func VerifyStorage(ctx context.Context, storage Storage, onerror func(id ID) bool) error {
	indexer, ok := storage.(IndexStorage)
	if !ok {
		return ErrNotIndexer
	}

	if ctx == nil {
		ctx = context.Background()
	}
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	ch, err := indexer.Index(ctx)
	if err != nil {
		return err
	}

	for id := range ch {
		ok, err := VerifyID(storage, id)
		if err != nil {
			return err
		}
		if !ok {
			ok = onerror(id)
			if !ok {
				return nil
			}
		}
	}

	return nil
}
