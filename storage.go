package cas

import (
	"bytes"
	"io"
	"io/ioutil"
	"strings"
)

// Storage represents an open content-addressable storage (CAS).  Adding items
// to the storage should be idempotent.  For example, inserting the same
// content twice should return the same id.
//
// The methods Create and Open should support concurrent access.
type Storage interface {
	// Close releases any resources linked to the storage.  Further calls to
	// any methods are an error.
	Close() error

	// Create copies data from the reader into the storage, and returns an ID
	// that can be used to identify the data.
	Create(src io.Reader) (ID, error)

	// Open locates the content associated with the ID, and returns a reader
	// that can be used to access the data.  If the object does not exist in
	// storage, then the error will be ErrNotExist.
	Open(id ID) (io.ReadCloser, error)
}

// CreateFromBytes is a utility function that inserts binary data into a
// storage.
func CreateFromBytes(s Storage, b []byte) (ID, error) {
	r := bytes.NewReader(b)
	return s.Create(r)
}

// CreateFromString is a utility function that inserts binary data into a
// storage.
func CreateFromString(s Storage, b string) (ID, error) {
	r := strings.NewReader(b)
	return s.Create(r)
}

// ReadAll reads all of the contents associated with the ID into a byte slice.
func ReadAll(s Storage, id ID) ([]byte, error) {
	r, err := s.Open(id)
	if err != nil {
		return nil, err
	}
	defer r.Close()

	return ioutil.ReadAll(r)
}

// VerifyID loads an object from the storage, and checks that the contents have
// not been corrupted.
func VerifyID(s Storage, id ID) (bool, error) {
	r, err := s.Open(id)
	if err != nil {
		return false, err
	}
	defer r.Close()

	hash := NewHash()
	_, err = io.Copy(hash, r)
	if err != nil {
		return false, err
	}

	newid := ID{}
	hash.Sum(newid[:0])
	return id == newid, nil
}
