package cas_test

import (
	"fmt"
	"io/ioutil"
	"testing"

	"gitlab.com/stone.code/cas"
	"gitlab.com/stone.code/cas/memory"
)

func ExampleStorage() {
	// Initialize an in-memory store.
	storage := memory.New()
	defer storage.Close()

	// Insert the data into the storage.
	id, err := cas.CreateFromString(storage, "Hello, world!")
	if err != nil {
		fmt.Println("error: ", err)
		return
	}

	// Find the storage based on the ID.
	r, err := storage.Open(id)
	if err != nil {
		fmt.Println("error: ", err)
		return
	}
	defer r.Close()

	data, err := ioutil.ReadAll(r)
	if err != nil {
		fmt.Println("error: ", err)
		return
	}
	fmt.Println(string(data))

	// Output:
	// Hello, world!
}

func TestCreateFromBytes(t *testing.T) {
	// Initialize an in-memory store.
	storage := memory.New()
	defer storage.Close()

	// Create a zero-length object using a string.
	id1, err := cas.CreateFromString(storage, "")
	if err != nil {
		t.Errorf("could not create from string: %s", err)
	}

	// Create a zero-length object using a byte slice.
	id2, err := cas.CreateFromBytes(storage, nil)
	if err != nil {
		t.Errorf("could not create from string: %s", err)
	}

	// The two objects should have the same ID.
	if id1 != id2 {
		t.Errorf("IDs do not match, got %v and %v", id1, id2)
	}
}

func TestReadAll(t *testing.T) {
	const content = "Hello, world!"

	t.Run("happy", func(t *testing.T) {
		// Initialize an in-memory store.
		storage := memory.New()
		defer storage.Close()

		id, err := cas.CreateFromString(storage, content)
		if err != nil {
			t.Fatalf("failed to store content: %s", err)
		}
		data, err := cas.ReadAll(storage, id)
		if err != nil {
			t.Errorf("could not read contents: %s", err)
		}
		if got := string(data); got != content {
			t.Errorf("want %v, got %v", content, got)
		}
	})

	t.Run("sad open", func(t *testing.T) {
		// Initialize an in-memory store.
		storage := &openfailStorage{}
		defer storage.Close()

		id, err := cas.CreateFromString(storage, content)
		if err != nil {
			t.Fatalf("failed to store content: %s", err)
		}
		_, err = cas.ReadAll(storage, id)
		if err != errMockOpen {
			t.Errorf("unexpected error: %s", err)
		}
	})

	t.Run("sad read", func(t *testing.T) {
		// Initialize an in-memory store.
		storage := &readfailStorage{}
		defer storage.Close()

		id, err := cas.CreateFromString(storage, content)
		if err != nil {
			t.Fatalf("failed to store content: %s", err)
		}
		_, err = cas.ReadAll(storage, id)
		if err != errMockRead {
			t.Errorf("unexpected error: %s", err)
		}
	})
}

func TestVerifyID(t *testing.T) {
	// Initialize an in-memory store.
	storage := memory.New()
	defer storage.Close()

	// Create a zero-length object using a string.
	id, err := cas.CreateFromString(storage, "Hello, world!")
	if err != nil {
		t.Errorf("could not create from string: %s", err)
	}

	// Verify zero-length object.
	ok, err := cas.VerifyID(storage, id)
	if err != nil {
		t.Errorf("could not verify the id %v : %s", id, err)
	}
	if !ok {
		t.Errorf("memory storage corruption detected")
	}

	// Verify a new, non-existent ID
	id[0]++
	_, err = cas.VerifyID(storage, id)
	if err != cas.ErrNotExist {
		t.Errorf("incorrect error code, got %v", err)
	}

	// Check that error in opening contents is reported.
	t.Run("sad open", func(t *testing.T) {
		_, err := cas.VerifyID(&openfailStorage{}, id)
		if err != errMockOpen {
			t.Errorf("got %v", err)
		}
	})

	// Check that error in reading contents is reported.
	t.Run("sad read", func(t *testing.T) {
		_, err := cas.VerifyID(&readfailStorage{}, id)
		if err != errMockRead {
			t.Errorf("got %v", err)
		}
	})

}
