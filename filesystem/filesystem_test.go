package filesystem_test

import (
	"io/ioutil"
	"os"
	"testing"

	"gitlab.com/stone.code/cas"
	"gitlab.com/stone.code/cas/castest"
	"gitlab.com/stone.code/cas/filesystem"
)

var (
	_ cas.Storage       = (*filesystem.Storage)(nil)
	_ cas.RemoveStorage = (*filesystem.Storage)(nil)
	_ cas.IndexStorage  = (*filesystem.Storage)(nil)
)

func TestOpen(t *testing.T) {
	cases := []struct {
		path string
		ok   bool
	}{
		{".", true},
		{"dummy", false},         // fail on non existent path
		{"filesystem.go", false}, // fail on path that is a regular file
	}

	for _, v := range cases {
		t.Run(v.path, func(t *testing.T) {
			fs, err := filesystem.Open(v.path)
			if ok := (err == nil); ok != v.ok {
				t.Errorf("want %v, got %v", v.ok, ok)
			}
			if err == nil {
				err = fs.Close()
				if err != nil {
					t.Errorf("error closing file system storage: %s", err)
				}
			}
		})
	}
}

func TestCreate(t *testing.T) {
	fs, close := testingStorage(t)
	defer close()

	castest.TestCreate(t, fs)
}

func TestIndex(t *testing.T) {
	fs, close := testingStorage(t)
	defer close()

	castest.TestIndex(t, fs)
}

func TestIndexCancel(t *testing.T) {
	fs, close := testingStorage(t)
	defer close()

	castest.TestIndexCancel(t, fs)
}

func TestRemove(t *testing.T) {
	fs, close := testingStorage(t)
	defer close()

	castest.TestRemove(t, fs)
}

func testingStorage(t *testing.T) (*filesystem.Storage, func()) {
	dir, err := ioutil.TempDir("", "gotest")
	if err != nil {
		t.Fatalf("could not create temp dir: %s", err)
	}

	fs, err := filesystem.Open(dir)
	if err != nil {
		t.Fatalf("could not open storage: %s", err)
	}

	return fs, func() {
		err := fs.Close()
		if err != nil {
			t.Errorf("could not close filesystem storage: %s", err)
		}
		err = os.RemoveAll(dir)
		if err != nil {
			t.Errorf("could not remove temp dir: %s", err)
		}
	}
}
