// Package filesystem provides a driver for content addressable storage (CAS)
// that uses the file system for backing.
package filesystem
