package filesystem

import "gitlab.com/stone.code/cas"

// Error records errors that are raised by the file system storage in addition
// to those created in gitlab.com/stone.code/cas.
const (
	ErrNotADirectory cas.Error = "path for storage must be a directory"
)
