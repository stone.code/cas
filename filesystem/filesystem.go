package filesystem

import (
	"context"
	"encoding/hex"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"

	"gitlab.com/stone.code/cas"
)

// Storage implements a driver for content-addressable storage (CAS) using the
// filesystem as the backing.
type Storage struct {
	path string
	file *os.File
}

// Open initializes a new content-addressed storage driver that uses the
// filesystem for backing.
func Open(path string) (*Storage, error) {
	path, err := filepath.Abs(path)
	if err != nil {
		return nil, err
	}

	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	stat, err := file.Stat()
	if err != nil {
		file.Close()
		return nil, err
	}
	if !stat.IsDir() {
		file.Close()
		return nil, &os.PathError{
			Op:   "open",
			Path: path,
			Err:  ErrNotADirectory,
		}
	}

	return &Storage{
		path: path,
		file: file,
	}, nil
}

// Close releases any resources linked to the storage.  Further calls to
// any methods are an error.
func (fs *Storage) Close() error {
	return fs.file.Close()
}

// FilenameForID returns the absolute path for the file holding the contents
// for the specified ID.
//
// Warning, modification of the contents of the file will invalidate the
// invariants of the file system storage.
func (fs *Storage) FilenameForID(id cas.ID) string {
	return filepath.Join(fs.path, hex.EncodeToString(id[0:4]), hex.EncodeToString(id[4:8]), hex.EncodeToString(id[8:]))
}

// Open locates the content associated with the ID, and returns a reader
// that can be used to access the data.
func (fs *Storage) Open(id cas.ID) (io.ReadCloser, error) {
	file, err := os.Open(fs.FilenameForID(id))
	if err != nil {
		if os.IsNotExist(err) {
			return nil, cas.ErrNotExist
		}
		return nil, err
	}
	return file, nil
}

func removeTempFile(file *os.File) error {
	name := file.Name()
	err := file.Close()
	if err != nil {
		return err
	}
	return os.Remove(name)
}

// Create copies data from the reader into the storage, and returns an ID
// that can be used to identify the data.
func (fs *Storage) Create(src io.Reader) (cas.ID, error) {
	// Need a file on the same filesystem as the storage
	file, err := ioutil.TempFile(fs.path, "tmp")
	if err != nil {
		return cas.ID{}, err
	}

	// Copy contents to the temp file, while also calculating the ID
	hash := cas.NewHash()
	_, err = io.Copy(io.MultiWriter(file, hash), src)
	if err != nil {
		removeTempFile(file)
		return cas.ID{}, err
	}

	id := cas.ID{}
	hash.Sum(id[:0])

	newname := fs.FilenameForID(id)
	if fileExists(newname) {
		removeTempFile(file)
		return id, nil
	}

	err = os.MkdirAll(filepath.Dir(newname), 0750)
	if err != nil {
		removeTempFile(file)
		return cas.ID{}, err
	}

	oldname := file.Name()
	err = file.Close()
	if err != nil {
		removeTempFile(file)
		return cas.ID{}, err
	}
	err = os.Rename(oldname, newname)
	if err != nil {
		os.Remove(oldname)
		return cas.ID{}, err
	}
	return id, nil
}

func fileExists(name string) bool {
	file, err := os.Open(name)
	if err != nil {
		return !os.IsNotExist(err)
	}
	file.Close()
	return true
}

// Remove deletes the object from the storage.  Future calls to open will
// return cas.ErrNotExist.
func (fs *Storage) Remove(id cas.ID) error {
	err := os.Remove(fs.FilenameForID(id))
	if err != nil && os.IsNotExist(err) {
		return nil
	}
	return err
}

// Index will concurrently return all of the IDs currently in the storage.
// The completeness or accuracy of the list is undefined when mixed with
// concurrent access to the storage.
func (fs *Storage) Index(ctx context.Context) (<-chan cas.ID, error) {
	if ctx == nil {
		ctx = context.Background()
	}

	ch := make(chan cas.ID)

	// Read the names from the directory.
	// Note:  We need to seek back to the start afterwards so that any future
	// calls to Index will be able to read the names anew.
	names, err := fs.file.Readdirnames(-1)
	if err != nil {
		return nil, err
	}
	_, err = fs.file.Seek(0, os.SEEK_SET)
	if err != nil {
		return nil, err
	}

	go func() {
		defer close(ch)

		for _, v := range names {
			id := cas.ID{}
			if len(v) == 8 {
				_, err := hex.Decode(id[0:4], []byte(v))
				if err == nil {
					ok := walkDir1(ctx, ch, filepath.Join(fs.path, v), &id)
					if !ok {
						return
					}
				}
			}
		}
	}()

	return ch, nil
}

func walkDir1(ctx context.Context, ch chan<- cas.ID, path string, id *cas.ID) bool {
	file, err := os.Open(path)
	if err != nil {
		return true
	}
	defer file.Close()

	names, err := file.Readdirnames(-1)
	if err != nil {
		return true
	}

	for _, v := range names {
		if len(v) == 8 {
			_, err := hex.Decode(id[4:8], []byte(v))
			if err == nil {
				ok := walkDir2(ctx, ch, filepath.Join(path, v), id)
				if !ok {
					return false
				}
			}
		}
	}

	return true
}

func walkDir2(ctx context.Context, ch chan<- cas.ID, path string, id *cas.ID) bool {
	file, err := os.Open(path)
	if err != nil {
		return true
	}
	defer file.Close()

	names, err := file.Readdirnames(-1)
	if err != nil {
		return true
	}

	for _, v := range names {
		if len(v) == 24 {
			_, err := hex.Decode(id[8:], []byte(v))
			if err == nil {
				select {
				case <-ctx.Done():
					return false
				case ch <- *id:
					// do nothing
				}
			}
		}
	}

	return true
}
