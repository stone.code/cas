package memory

import (
	"bytes"
	"context"
	"io"
	"sync"

	"gitlab.com/stone.code/cas"
)

// Storage implements a driver for content-addressable storage (CAS) using
// memory as the backing.
type Storage struct {
	contents map[cas.ID][]byte
	mu       sync.Mutex
}

// New returns a new content-addressed storage driver that uses memory for
// the backing.
func New() *Storage {
	return &Storage{
		contents: map[cas.ID][]byte{},
	}
}

// Close releases any resources linked to the storage.  Further calls to
// any methods are an error.
func (mem *Storage) Close() error {
	mem.contents = nil
	return nil
}

// Open locates the content associated with the ID, and returns a reader
// that can be used to access the data.
func (mem *Storage) Open(id cas.ID) (io.ReadCloser, error) {
	mem.mu.Lock()
	defer mem.mu.Unlock()

	if data, ok := mem.contents[id]; ok {
		return NewReader(data), nil
	}
	return nil, cas.ErrNotExist
}

// Create copies data from the reader into the storage, and returns an ID
// that can be used to identify the data.
func (mem *Storage) Create(src io.Reader) (cas.ID, error) {
	// Copy contents to the temp file, while also calculating the ID
	data := bytes.NewBuffer(nil)
	hash := cas.NewHash()
	_, err := io.Copy(io.MultiWriter(data, hash), src)
	if err != nil {
		return cas.ID{}, err
	}

	id := cas.ID{}
	hash.Sum(id[:0])

	if _, ok := mem.contents[id]; ok {
		return id, nil
	}

	mem.mu.Lock()
	defer mem.mu.Unlock()

	mem.contents[id] = data.Bytes()
	return id, nil
}

// Remove deletes the object from the storage.  Future calls to open will
// return cas.ErrNotExist.
func (mem *Storage) Remove(id cas.ID) error {
	mem.mu.Lock()
	defer mem.mu.Unlock()

	delete(mem.contents, id)

	return nil
}

// Index will concurrently return all of the IDs currently in the storage.
// The completeness or accuracy of the list is undefined when mixed with
// concurrent access to the storage.
func (mem *Storage) Index(ctx context.Context) (<-chan cas.ID, error) {
	if ctx == nil {
		ctx = context.Background()
	}

	ch := make(chan cas.ID, 128 /* optimize? */)

	go func() {
		defer close(ch)

		for id := range mem.contents {
			select {
			case <-ctx.Done():
				return
			case ch <- id:
				// do nothing
			}
		}
	}()

	return ch, nil
}
