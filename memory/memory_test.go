package memory_test

import (
	"testing"

	"gitlab.com/stone.code/cas"
	"gitlab.com/stone.code/cas/castest"
	"gitlab.com/stone.code/cas/memory"
)

var (
	_ cas.Storage       = (*memory.Storage)(nil)
	_ cas.RemoveStorage = (*memory.Storage)(nil)
	_ cas.IndexStorage  = (*memory.Storage)(nil)
)

func TestCreate(t *testing.T) {
	fs := memory.New()
	defer fs.Close()

	castest.TestCreate(t, fs)
}

func TestIndex(t *testing.T) {
	fs := memory.New()
	defer fs.Close()

	castest.TestIndex(t, fs)
}

func TestIndexCancel(t *testing.T) {
	fs := memory.New()
	defer fs.Close()

	castest.TestIndexCancel(t, fs)
}

func TestRemove(t *testing.T) {
	fs := memory.New()
	defer fs.Close()

	castest.TestRemove(t, fs)
}
