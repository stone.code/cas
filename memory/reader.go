package memory

import (
	"errors"
	"io"
)

// A Reader implements the io.ReadCLoser on a bytes slice.
type Reader struct {
	s []byte
	i int64 // current reading index
}

// NewReader returns a new Reader reading from b.
func NewReader(b []byte) *Reader {
	return &Reader{b, 0}
}

// Close closes the Reader, rendering it unusable for I/O.
func (r *Reader) Close() error {
	r.s = nil
	return nil
}

// Read implements the io.Reader interface.
func (r *Reader) Read(b []byte) (n int, err error) {
	if r.i >= int64(len(r.s)) {
		return 0, io.EOF
	}
	n = copy(b, r.s[r.i:])
	r.i += int64(n)
	return
}

// Seek implements the io.Seeker interface.
func (r *Reader) Seek(offset int64, whence int) (int64, error) {
	var abs int64
	switch whence {
	case io.SeekStart:
		abs = offset
	case io.SeekCurrent:
		abs = r.i + offset
	case io.SeekEnd:
		abs = int64(len(r.s)) + offset
	default:
		return 0, errors.New("memory.Reader.Seek: invalid whence")
	}
	if abs < 0 {
		return 0, errors.New("memory.Reader.Seek: negative position")
	}
	r.i = abs
	return abs, nil
}
