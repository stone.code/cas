// Package memory provides a driver for content addressible storage (CAS)
// that uses memory for backing.
package memory
