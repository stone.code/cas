package castest

import (
	"gitlab.com/stone.code/cas"
)

const (
	errReadFail cas.Error = "mock read failed"
)

type readfailReader struct{}

func (*readfailReader) Read([]byte) (int, error) {
	return 0, errReadFail
}
