package castest

import (
	"bytes"
	"sort"

	"gitlab.com/stone.code/cas"
)

func sortIDs(ids []cas.ID) []cas.ID {
	sort.Slice(ids, func(i, j int) bool {
		return bytes.Compare(ids[i][:], ids[j][:]) < 0
	})
	return ids
}
