package castest

import (
	"bytes"
	"context"
	"reflect"
	"sync"
	"testing"
	"testing/quick"

	"gitlab.com/stone.code/cas"
)

var (
	cases = []struct {
		content string
		id      cas.ID
	}{
		{"", cas.ID{0xda, 0x39, 0xa3, 0xee, 0x5e, 0x6b, 0x4b, 0x0d, 0x32, 0x55, 0xbf, 0xef, 0x95, 0x60, 0x18, 0x90, 0xaf, 0xd8, 0x07, 0x09}},
		{"Hello, world!", cas.ID{0x94, 0x3a, 0x70, 0x2d, 0x06, 0xf3, 0x45, 0x99, 0xae, 0xe1, 0xf8, 0xda, 0x8e, 0xf9, 0xf7, 0x29, 0x60, 0x31, 0xd6, 0x99}},
		{"0xdeadbeaf", cas.ID{0xa0, 0xf2, 0x35, 0xd6, 0x8c, 0xce, 0x3a, 0xe7, 0x4c, 0x7f, 0xcb, 0xad, 0x60, 0xea, 0xdc, 0x22, 0x61, 0x52, 0xcb, 0x3d}},
		{"\x00\x00\x00\x00", cas.ID{0x90, 0x69, 0xca, 0x78, 0xe7, 0x45, 0x0a, 0x28, 0x51, 0x73, 0x43, 0x1b, 0x3e, 0x52, 0xc5, 0xc2, 0x52, 0x99, 0xe4, 0x73}},
	}
)

// Cases returns a slice with all of the IDs used when initializing a storage
// driver.
func Cases() []cas.ID {
	ids := make([]cas.ID, 0, len(cases))
	for _, v := range cases {
		ids = append(ids, v.id)
	}
	sortIDs(ids)
	return ids
}

// TestCreate tests that the methods Create and Open respect the invariants
// for the Storage interface.
func TestCreate(t *testing.T, storage cas.Storage) {
	for _, v := range cases {
		t.Run(v.content, func(t *testing.T) {
			// Create the object
			id, err := storage.Create(bytes.NewBuffer([]byte(v.content)))
			if err != nil {
				t.Errorf("error creating object: %s", err)
			}
			if id != v.id {
				t.Errorf("want %v, got %v", v.id, id)
			}

			// verify the contents
			data, err := cas.ReadAll(storage, id)
			if err != nil {
				t.Errorf("error reading object: %s", err)
			}
			if got := string(data); got != v.content {
				t.Errorf("want %v, got %v", v.content, got)
			}

			// Store a second time.  Storing an object should be idempotent
			id, err = storage.Create(bytes.NewBuffer([]byte(v.content)))
			if err != nil {
				t.Errorf("error creating object: %s", err)
			}
			if id != v.id {
				t.Errorf("want %v, got %v", v.id, id)
			}
		})
	}

	if !testing.Short() {
		t.Run("quicktest", func(t *testing.T) {
			f := func(x []byte) bool {
				// Create the object
				id, err := storage.Create(bytes.NewBuffer(x))
				if err != nil {
					t.Logf("error creating object: %s", err)
					return false
				}

				// verify the contents
				data, err := cas.ReadAll(storage, id)
				if err != nil {
					t.Logf("error creating object: %s", err)
					return false
				}
				return bytes.Compare(x, data) == 0
			}
			if err := quick.Check(f, nil); err != nil {
				t.Error(err)
			}
		})
	}

	t.Run("sad read", func(t *testing.T) {
		// Create the object
		id, err := storage.Create(&readfailReader{})
		if err != errReadFail {
			t.Errorf("unexpected error: %v", err)
		}
		if !id.IsZero() {
			t.Errorf("unexpected error: %v", id)
		}
	})

	t.Run("parallel", func(t *testing.T) {
		wg := sync.WaitGroup{}
		wg.Add(len(cases))
		defer wg.Wait()

		for _, v := range cases {
			v := v

			go func() {
				defer wg.Done()

				// Create the object
				id, err := storage.Create(bytes.NewBuffer([]byte(v.content)))
				if err != nil {
					t.Errorf("error creating object: %s", err)
				}
				if id != v.id {
					t.Errorf("want %v, got %v", v.id, id)
				}

				// verify the contents
				data, err := cas.ReadAll(storage, id)
				if err != nil {
					t.Errorf("error reading object: %s", err)
				}
				if got := string(data); got != v.content {
					t.Errorf("want %v, got %v", v.content, got)
				}

				// Store a second time.  Storing an object should be idempotent
				id, err = storage.Create(bytes.NewBuffer([]byte(v.content)))
				if err != nil {
					t.Errorf("error creating object: %s", err)
				}
				if id != v.id {
					t.Errorf("want %v, got %v", v.id, id)
				}
			}()
		}
	})
}

// TestIndex tests that the method Index respects the invariants for the
// Indexer interface.  Obviously, the storage passed must also respect the
// Indexer interface.
func TestIndex(t *testing.T, storage cas.Storage) {
	// Create the objects
	for _, v := range cases {
		id, err := storage.Create(bytes.NewBuffer([]byte(v.content)))
		if err != nil {
			t.Fatalf("error creating object: %s", err)
		}
		if id != v.id {
			t.Fatalf("want %v, got %v", v.id, id)
		}
	}

	// Index the objects, and ensure that the list matches
	ch, err := storage.(cas.IndexStorage).Index(nil)
	if err != nil {
		t.Fatalf("could not list IDs: %s", err)
	}
	got := func() []cas.ID {
		ids := []cas.ID{}
		for id := range ch {
			ids = append(ids, id)
		}
		sortIDs(ids)
		return ids
	}()
	want := Cases()
	if !reflect.DeepEqual(got, want) {
		t.Errorf("want %v, got %v", want, got)
	}
}

// TestIndexCancel tests that the method Index respects the invariants for the
// Indexer interface.  This tests cancels the indexing operation after half
// of the IDs have been processed.  Obviously, the storage passed must also
// respect the Indexer interface.
func TestIndexCancel(t *testing.T, storage cas.Storage) {
	// Create the objects
	for _, v := range cases {
		id, err := storage.Create(bytes.NewBuffer([]byte(v.content)))
		if err != nil {
			t.Fatalf("error creating object: %s", err)
		}
		if id != v.id {
			t.Fatalf("want %v, got %v", v.id, id)
		}
	}

	// Create a context that we can cancel.
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Index the objects, and ensure that the list matches
	ch, err := storage.(cas.IndexStorage).Index(ctx)
	if err != nil {
		t.Fatalf("could not list IDs: %s", err)
	}
	got := func() []cas.ID {
		ids := []cas.ID{}
		cnt := 0
		for id := range ch {
			ids = append(ids, id)

			cnt++
			if cnt >= len(cases)/2 {
				cancel()

				// Note race condition.
				// The goroutine feeding the channel may concurrently put
				// an ID into the channel while we cancel.
				// Need to break out of loop as well to control length of IDs.
				break
			}
		}
		sortIDs(ids)
		return ids
	}()

	// Convert the list of IDs that could be present into a set.
	want := map[cas.ID]struct{}{}
	for _, v := range Cases() {
		want[v] = struct{}{}
	}

	// We cancelled halfway though, so the number of IDs in our list should be
	// half of those put into storage.
	if len(got) != len(want)/2 {
		t.Errorf("want length %v, got length %v", len(want)/2, len(got))
	}

	// Every ID in the returned list should be in the set of objects put into
	// storage.
	for _, v := range got {
		if _, ok := want[v]; !ok {
			t.Errorf("found unexpected ID in results: %v", v)
		}
	}
}

// TestRemove tests that the method Remove respects the invariants for the
// Remover interface.  Obviously, the storage passed must also respect the
// Remover interface.
func TestRemove(t *testing.T, storage cas.Storage) {
	id, err := storage.Create(bytes.NewBuffer(nil))
	if err != nil {
		t.Errorf("could not create object: %s", err)
	}
	err = storage.(cas.RemoveStorage).Remove(id)
	if err != nil {
		t.Errorf("could not remove object: %s", err)
	}
	_, err = storage.Open(id)
	if err != cas.ErrNotExist {
		t.Errorf("want %v, got %v", cas.ErrNotExist, err)
	}
	err = storage.(cas.RemoveStorage).Remove(id)
	if err != nil {
		t.Errorf("could not remove object: %s", err)
	}
}
