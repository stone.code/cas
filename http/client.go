package http

import (
	"bufio"
	"context"
	"io"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/stone.code/cas"
)

// Storage implements a driver for content-addressable storage (CAS) using the
// filesystem as the backing.
type Storage struct {
	url    string
	client *http.Client
}

// NewClient initializes a new content-addressed storage driver for accessing
// content over HTTP.
func NewClient(url string) (*Storage, error) {
	client := http.DefaultClient

	if !strings.HasSuffix(url, "/") {
		url = url + "/"
	}

	resp, err := client.Get(url + "id")
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, ErrEndpoint
	}

	body, err := ioutil.ReadAll(&io.LimitedReader{N: 128, R: resp.Body})
	if err != nil {
		return nil, err
	}
	if string(body) != "gitlab.com/stone.code/cas/http" {
		return nil, ErrEndpoint
	}

	return &Storage{
		url:    url,
		client: client,
	}, nil
}

// Close releases any resources linked to the storage.  Further calls to
// any methods are an error.
func (s *Storage) Close() error {
	s.client = nil
	return nil
}

// Open locates the content associated with the ID, and returns a reader
// that can be used to access the data.
func (s *Storage) Open(id cas.ID) (io.ReadCloser, error) {
	resp, err := s.client.Get(s.url + id.String())
	if err != nil {
		return nil, err
	}
	if resp.StatusCode == http.StatusNotFound {
		resp.Body.Close()
		return nil, cas.ErrNotExist
	}
	if resp.StatusCode != http.StatusOK {
		resp.Body.Close()
		return nil, ErrHTTP
	}

	return resp.Body, nil
}

// Create copies data from the reader into the storage, and returns an ID
// that can be used to identify the data.
func (s *Storage) Create(src io.Reader) (cas.ID, error) {
	nop := &noCloseReader{R: src}
	resp, err := s.client.Post(s.url, "application/octet-stream", nop)
	if err != nil {
		// Don't want to confuse error on the reader with HTTP errors.
		if e2 := nop.Err(); e2 != nil {
			err = e2
		}
		return cas.ID{}, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusAccepted {
		resp.Body.Close()
		return cas.ID{}, ErrHTTP
	}

	body, err := ioutil.ReadAll(&io.LimitedReader{N: 128, R: resp.Body})
	if err != nil {
		return cas.ID{}, ErrHTTP
	}
	id, err := cas.ParseID(string(body))
	if err != nil {
		return cas.ID{}, ErrHTTP
	}

	return id, nil
}

// Remove deletes the object from the storage.  Future calls to open will
// return cas.ErrNotExist.
func (s *Storage) Remove(id cas.ID) error {
	req, err := http.NewRequest("DELETE", s.url+id.String(), nil)
	if err != nil {
		return err
	}
	resp, err := s.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if sc := resp.StatusCode; sc != http.StatusOK && sc != http.StatusAccepted && sc != http.StatusNoContent {
		return ErrHTTP
	}
	return nil
}

// Index will concurrently return all of the IDs currently in the storage.
// The completeness or accuracy of the list is undefined when mixed with
// concurrent access to the storage.
func (s *Storage) Index(ctx context.Context) (<-chan cas.ID, error) {
	if ctx == nil {
		ctx = context.Background()
	}

	resp, err := s.client.Get(s.url + "index")
	if err != nil {
		return nil, err
	}
	// Note missing defer to close body.
	// Will be closed in the goroutine.
	if resp.StatusCode != http.StatusOK {
		resp.Body.Close()
		return nil, ErrHTTP
	}

	ch := make(chan cas.ID)

	go func() {
		defer func() {
			close(ch)
			resp.Body.Close()
		}()

		scanner := bufio.NewScanner(resp.Body)
		for scanner.Scan() {
			id, err := cas.ParseID(scanner.Text())
			if err != nil {
				// TODO:  no ability to report error
				continue
			}

			select {
			case <-ctx.Done():
				return
			case ch <- id:
				// do nothing
			}
		}
		if err := scanner.Err(); err != nil {
			// TODO:  no ability to report error
		}
	}()

	return ch, nil
}
