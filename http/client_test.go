package http_test

import (
	"net/http/httptest"
	"testing"

	"gitlab.com/stone.code/cas/castest"
	"gitlab.com/stone.code/cas/http"
	"gitlab.com/stone.code/cas/memory"
)

func TestCreate(t *testing.T) {
	fs, close := testingStorage(t)
	defer close()

	castest.TestCreate(t, fs)
}

func TestIndex(t *testing.T) {
	fs, close := testingStorage(t)
	defer close()

	castest.TestIndex(t, fs)
}

func TestIndexCancel(t *testing.T) {
	fs, close := testingStorage(t)
	defer close()

	castest.TestIndexCancel(t, fs)
}

func TestRemove(t *testing.T) {
	fs, close := testingStorage(t)
	defer close()

	castest.TestRemove(t, fs)
}

func testingStorage(t *testing.T) (*http.Storage, func()) {
	// Build an in-memory storage for backing
	mem := memory.New()

	// Build a server
	mux := http.NewServeMux(mem)
	mux.SetAllowRemove(true)
	server := httptest.NewServer(mux)

	// Build the client
	fs, err := http.NewClient(server.URL)
	if err != nil {
		t.Fatalf("could not initialize HTTP client: %s", err)
	}

	return fs, func() {
		fs.Close()
		server.Close()
		mem.Close()
	}
}
