package http

import (
	"gitlab.com/stone.code/cas"
)

// Error records errors that are raised by the HTTP storage in addition
// to those created in gitlab.com/stone.code/cas.
const (
	ErrEndpoint cas.Error = "endpoint for HTTP storage not recognized"
	ErrHTTP     cas.Error = "error in HTTP transport"
)
