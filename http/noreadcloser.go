package http

import "io"

// A noCloseReader wraps an existing reader, put prevents any callees from
// accessing the Close method on the concrete type.  This is used so that
// an io.Reader can be passed to net.Client.Post without closing close on the
// concrete type.
type noCloseReader struct {
	R   io.Reader
	err error
}

func (nop *noCloseReader) Read(p []byte) (int, error) {
	n, err := nop.R.Read(p)
	if err != nil {
		nop.err = err
	}
	return n, err
}

func (nop *noCloseReader) Err() error {
	return nop.err
}
