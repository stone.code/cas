package http

import (
	"io"
	"net/http"
	"time"

	"gitlab.com/stone.code/cas"
)

// A ServeMux provides an HTTP request multiplexer that provides a REST
// interface for a content-addressed storage driver.
type ServeMux struct {
	s           cas.Storage
	i           cas.IndexStorage
	r           cas.RemoveStorage
	allowRemove bool
}

// NewServeMux initializes a new request multiplexer to server requests to
// the provides storage driver.  The multiplexer will also determine if the
// driver support indexing and removing of the content.
//
// The REST end-points to remove content are disabled.
func NewServeMux(storage cas.Storage) *ServeMux {
	indexer, _ := storage.(cas.IndexStorage)
	remover, _ := storage.(cas.RemoveStorage)

	return &ServeMux{
		s: storage,
		i: indexer,
		r: remover,
	}
}

// SetAllowRemove updates the flag that allows (or disallows) clients to remove
// content from the storage driver.  If the driver does not support removal,
// then the end-points will remain disabled.
func (s *ServeMux) SetAllowRemove(flag bool) {
	s.allowRemove = flag && (s.r != nil)
}

// ServeHTTP implements http.Handler.
func (s *ServeMux) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	if req.URL.Path == "" {
		http.NotFound(w, req)
		return
	}

	if id, err := cas.ParseID(req.URL.Path[1:]); err == nil {
		if req.Method == "GET" || req.Method == "HEAD" {
			s.open(w, req, id)
			return
		}
		if req.Method == "DELETE" && s.allowRemove {
			s.remove(w, req, id)
			return
		}

		http.Error(w, "405 method not allowed", http.StatusMethodNotAllowed)
		return
	}

	if req.URL.Path == "/" {
		if req.Method == "POST" {
			s.create(w, req)
			return
		}

		http.Error(w, "405 method not allowed", http.StatusMethodNotAllowed)
		return
	}

	if req.URL.Path == "/index" {
		if req.Method == "GET" {
			s.index(w, req)
			return
		}

		http.Error(w, "405 method not allowed", http.StatusMethodNotAllowed)
		return
	}

	if req.URL.Path == "/id" {
		w.Write([]byte("gitlab.com/stone.code/cas/http"))
		return
	}

	http.NotFound(w, req)
}

func (s *ServeMux) create(w http.ResponseWriter, req *http.Request) {
	id, err := s.s.Create(req.Body)
	if err != nil {
		http.Error(w, "500 internal server error", http.StatusInternalServerError)
		return
	}

	w.Header().Add("Location", id.String()) // should be absolute URI
	w.WriteHeader(http.StatusAccepted)
	w.Write([]byte(id.String()))
}

func (s *ServeMux) open(w http.ResponseWriter, req *http.Request, id cas.ID) {
	r, err := s.s.Open(id)
	if err == cas.ErrNotExist {
		http.NotFound(w, req)
		return
	} else if err != nil {
		http.Error(w, "500 internal server error", http.StatusInternalServerError)
		return
	}

	if rs, ok := r.(io.ReadSeeker); ok {
		http.ServeContent(w, req, req.URL.Path, time.Time{}, rs)
	} else {
		io.Copy(w, r)
	}
}

func (s *ServeMux) remove(w http.ResponseWriter, req *http.Request, id cas.ID) {
	err := s.r.Remove(id)
	if err != nil {
		http.Error(w, "500 internal server error", http.StatusInternalServerError)
		return
	}
	http.Error(w, "204 no content", http.StatusNoContent)
}

func (s *ServeMux) index(w http.ResponseWriter, req *http.Request) {
	ch, err := s.i.Index(req.Context())
	if err != nil {
		http.Error(w, "500 internal server error", http.StatusInternalServerError)
		return
	}

	for id := range ch {
		w.Write(append([]byte(id.String()), '\n'))
	}
}
