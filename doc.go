// Package cas provides a framework for working with content addressible
// storage (CAS).  This package only provides the interface, as several
// types of drivers are possible.
package cas
