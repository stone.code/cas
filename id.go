package cas

import (
	"crypto/sha1"
	"encoding/hex"
	"hash"
)

// An ID identifies a piece of content, which can be any binary blob.
// The ID is a checksum of the content, ensuring that identical data is always
// mapped to the same ID.
type ID [sha1.Size]byte

// IsZero returns true if the ID is the zero object.
func (id ID) IsZero() bool {
	return id == ID{}
}

// String returns a human readable representation of the ID.
func (id ID) String() string {
	buf := [sha1.Size*2 + 2]byte{}
	hex.Encode(buf[:8], id[:4])
	buf[8] = '-'
	hex.Encode(buf[9:17], id[4:8])
	buf[17] = '-'
	hex.Encode(buf[18:], id[8:])
	return string(buf[:])
}

// NewID calculates the ID for a binary blob.
func NewID(data []byte) ID {
	return sha1.Sum(data)
}

// ParseID returns the ID that corresponds to the string.
func ParseID(id string) (ID, error) {
	if len(id) != 42 || id[8] != '-' || id[17] != '-' {
		return ID{}, &parseError{id}
	}

	tmp := []byte(id)
	out := ID{}
	if _, err := hex.Decode(out[0:4], tmp[:8]); err != nil {
		return ID{}, err
	}
	if _, err := hex.Decode(out[4:8], tmp[9:17]); err != nil {
		return ID{}, err
	}
	if _, err := hex.Decode(out[8:], tmp[18:]); err != nil {
		return ID{}, err
	}
	return out, nil
}

type parseError struct {
	id string
}

func (pe *parseError) Error() string {
	return "parsing \"" + pe.id + "\" as ID: invalid syntax"
}

// NewHash returns a new hash.Hash computing the checksum.
func NewHash() hash.Hash {
	return sha1.New()
}
