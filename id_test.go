package cas_test

import (
	"testing"
	"testing/quick"

	"gitlab.com/stone.code/cas"
)

func TestNewID(t *testing.T) {
	cases := []struct {
		in  string
		out cas.ID
	}{
		{"", cas.ID{0xda, 0x39, 0xa3, 0xee, 0x5e, 0x6b, 0x4b, 0x0d, 0x32, 0x55, 0xbf, 0xef, 0x95, 0x60, 0x18, 0x90, 0xaf, 0xd8, 0x07, 0x09}},
		{"Hello, world!", cas.ID{0x94, 0x3a, 0x70, 0x2d, 0x06, 0xf3, 0x45, 0x99, 0xae, 0xe1, 0xf8, 0xda, 0x8e, 0xf9, 0xf7, 0x29, 0x60, 0x31, 0xd6, 0x99}},
	}

	for _, v := range cases {
		t.Run(v.in, func(t *testing.T) {
			if out := cas.NewID([]byte(v.in)); out != v.out {
				t.Errorf("wanted %v, got %v", v.out, out)
			}
		})
	}
}

func TestParseID(t *testing.T) {
	cases := []struct {
		in string
		ok bool
	}{
		{"da39a3ee-5e6b4b0d-3255bfef95601890afd80709", true},
		{"da39a3ee 5e6b4b0d-3255bfef95601890afd80709", false},
		{"da39a3ee-5e6b4b0d 3255bfef95601890afd80709", false},
		{"da39a3ee-5e6b4b0d-3255bfef95601890afd807093", false},
		{"da39a3ee-5e6b4b0d-3255bfef95601890afd8070", false},
		{"$a39a3ee-5e6b4b0d-3255bfef95601890afd80709", false},
		{"da39a3ee-5e6b$b0d-3255bfef95601890afd80709", false},
		{"da39a3ee-5e6b4b0d-3255bfef95601890afd8070$", false},
	}

	for _, v := range cases {
		t.Run(v.in, func(t *testing.T) {
			_, err := cas.ParseID(v.in)
			if ok := (err == nil); ok != v.ok {
				t.Errorf("wanted %v, got %v", v.ok, ok)
			}
			if err != nil && err.Error() == "" {
				t.Errorf("empty error message")
			}
		})
	}
	f := func(x cas.ID) bool {
		str := x.String()
		y, err := cas.ParseID(str)
		return err == nil && x == y
	}
	if err := quick.Check(f, nil); err != nil {
		t.Error(err)
	}
}

func TestIDIsZero(t *testing.T) {
	cases := []struct {
		id     cas.ID
		iszero bool
	}{
		{cas.ID{}, true},
		{cas.ID{0xda, 0x39, 0xa3, 0xee, 0x5e, 0x6b, 0x4b, 0x0d, 0x32, 0x55, 0xbf, 0xef, 0x95, 0x60, 0x18, 0x90, 0xaf, 0xd8, 0x07, 0x09}, false},
		{cas.ID{0x94, 0x3a, 0x70, 0x2d, 0x06, 0xf3, 0x45, 0x99, 0xae, 0xe1, 0xf8, 0xda, 0x8e, 0xf9, 0xf7, 0x29, 0x60, 0x31, 0xd6, 0x99}, false},
	}

	for _, v := range cases {
		if iszero := v.id.IsZero(); iszero != v.iszero {
			t.Errorf("%v: want %v, got %v", v.id, v.iszero, iszero)
		}
	}
}
